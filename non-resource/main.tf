
resource "docker_image" "image_id" {
##  name = "ghost:latest"
  name = "${var.image}"
}

resource "docker_container" "container_id" {
##  name = "blog"
  name = "${var.container_name}"
  image = "${docker_image.image_id.latest}"
  ports {
    internal = "${var.int_port}"
    external = "${var.ext_port}"
  }

}

resource "null_resource" "null_id" {
  provisioner "local-exec" {
      command = "echo ${docker_container.container_id.ip_address} >> container.txt"
  }
}

variable "image" {
  description = "image for container"
  default = "ubuntu:xenial"
}

variable "container1" {
  description = "Name of master"
  default = "master"
}

variable "container2" {
  description = "Name of agent1"
  default = "agent1"
}


variable "int_port" {
  description = "Internal port for Container"
  default = "2368"
}

variable "ext_port" {
  description = "External port for Container"
  default = "80"
}


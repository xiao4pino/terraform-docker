# Docker Provider
resource "docker_image" "ubuntu" {
##  name = "ghost:latest"
  name = "${var.image}"
}

resource "docker_container" "master" {
##  name = "blog"
  name = "${var.container1}"
  image = "${docker_image.ubuntu.latest}"
  
  
  ports {
    internal = "${var.int_port}"
    external = "${var.ext_port}"
  }

}

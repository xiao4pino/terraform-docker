resource "null_resource" "null_id" {
 
  connection {
    type = "ssh"
    user = "vagrant"
    host = "192.168.56.100"
    private_key = "${file("/home/ubuntu/hvc-vagrant/vgrt-terraform/.vagrant/machines/default/virtualbox/private_key")}"
    timeout = "30s"
#    script_path = "./rubytest.sh"
    script_path = "./*.*"
#    agent



  }  
        
    
  provisioner "local-exec" {
#    command = "echo ${docker_container.container_id.ip_address} >> container.txt"
    command = "date  >> provisioner.txt"
  }

  provisioner "remote-exec" {
    inline = [
      "date  >> provisioner.txt"
    ]

  }



   
}
